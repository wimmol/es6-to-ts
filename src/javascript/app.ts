import { createFighters } from './components/fightersView';
import { fighterService } from './services/fightersService';
import {twoFighters} from "./helpers/tsTypes";

class App {
    constructor() {
        this.startApp();
    }

    static rootElement: HTMLElement | null = document.getElementById('root');
    static loadingElement: HTMLElement | null = document.getElementById('loading-overlay');

    async startApp(): Promise<void> {
        try {
            if (App.loadingElement !== null)
                App.loadingElement.style.visibility = 'visible';

            const fighters: twoFighters = await fighterService.getFighters();
            const fightersElement: HTMLElement = createFighters(fighters);

            if (App.rootElement !== null)
                App.rootElement.appendChild(fightersElement);

        } catch (error) {
            console.warn(error);
            if (App.rootElement !== null)
                App.rootElement.innerText = 'Failed to load data';
        } finally {
            if (App.loadingElement !== null)
                App.loadingElement.style.visibility = 'hidden';
        }
    }
}

export default App;
