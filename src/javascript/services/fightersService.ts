import {callApi} from '../helpers/apiHelper';
import {IFightersData, twoFighters} from "../helpers/tsTypes";

class FighterService {
  async getFighters():Promise<twoFighters> {
    try {
      const endpoint = 'fighters.json';
      return await callApi(endpoint, 'GET');
    } catch (e) {
      throw {e};
    }
  }

  async getFighterDetails(id:string):Promise<IFightersData[]> {
    // todo: implement this method
    // endpoint - `details/fighter/${id}.json`;
    try {
      const endpoint = `details/fighter/${id}.json`;
      return await callApi(endpoint, 'GET');
    }
    catch (e) {
      throw {e};
    }
  }
}

export const fighterService = new FighterService();
