
export type twoFighters = [IFighter, IFighter];
export interface IFightersData {
    _id: string;
    name: string;
    source: string;
}
export interface IFighter extends IFightersData {
    health: number;
    attack: number;
    defense: number;
    source: string;
}
export interface IElementInfo {
    tagName: string;
    className?: string;
    attributes?: IAttributes;
}
export interface IAttributes {
    [name:string]:string;
}
export interface IConditions {
    isPlayerOneInBlock: boolean;
    isPlayerTwoInBlock: boolean;
    playerOneHealth: number;
    playerTwoHealth: number;
    playerOnePerHealth: number;
    playerTwoPerHealth: number;
    isPlayerOneCritReady: boolean;
    isPlayerTwoCritReady: boolean;
}
export type selector = (event: Event, fighterId: string) => Promise<void>;
export type closer = () => void;
