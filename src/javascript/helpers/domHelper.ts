import {IElementInfo} from "./tsTypes";

export function createElement({ tagName, className, attributes = {} }: IElementInfo): HTMLElement {
  const element: HTMLElement = document.createElement(tagName);

  if (className) {
    const classNames = className.split(' ').filter(Boolean);
    element.classList.add(...classNames);
  }
  Object.keys(attributes).forEach((key:string) => element.setAttribute(key, attributes[key]));

  return element;
}
