import {showModal} from "./modal";
import {createFighterImage} from "../fighterPreview";
import {IFighter} from "../../helpers/tsTypes";


export function showWinnerModal(fighter: IFighter): void {
    // call showModal function
    showModal( {
        title: `our winner is... !!!${fighter.name}!!!`,
        bodyElement: createFighterImage(fighter),
        onClose: () => {
            console.log(fighter.name);
        }
    });
}
