import { createElement } from '../../helpers/domHelper';
import {closer} from "../../helpers/tsTypes";

interface IModalAttributes {
  title: string;
  bodyElement: HTMLElement;
  onClose?: closer;
}

export function showModal({ title, bodyElement, onClose}: IModalAttributes): void {
  const root = getModalContainer();
  const modal = createModal({ title, bodyElement, onClose });
  if (root) {
    root.append(modal);
  }
}

function getModalContainer() {
  return document.getElementById('root');
}

function createModal({ title, bodyElement, onClose}: IModalAttributes): HTMLElement {
  const layer: HTMLElement = createElement({ tagName: 'div', className: 'modal-layer' });
  const modalContainer: HTMLElement = createElement({ tagName: 'div', className: 'modal-root' });
  const header: HTMLElement = createHeader(title, <closer>onClose);

  modalContainer.append(header, bodyElement);
  layer.append(modalContainer);

  return layer;
}

function createHeader(title:string, onClose: closer) {
  const headerElement: HTMLElement = createElement({ tagName: 'div', className: 'modal-header' });
  const titleElement: HTMLElement = createElement({ tagName: 'span' });
  const closeButton: HTMLElement = createElement({ tagName: 'div', className: 'close-btn' });
  
  titleElement.innerText = title;
  closeButton.innerText = '×';
  
  const close: closer = () => {
    hideModal();
    onClose();
  };
  closeButton.addEventListener('click', close);
  headerElement.append(titleElement, closeButton);
  
  return headerElement;
}

function hideModal(): void {
  const modal: Element = document.getElementsByClassName('modal-layer')[0];
  modal?.remove();
}
