import { createElement} from '../helpers/domHelper';
import { IFighter, IElementInfo, IAttributes} from "../helpers/tsTypes";

export function createFighterPreview(fighter: IFighter, position: 'left' | 'right'): HTMLElement {
  if (typeof fighter  ==  "undefined"){
    fighter =  {
      _id: "7",
      name: "Choose second  hero",
      health: 0,
      attack: 0,
      defense: 0,
      source: "https://i.imgur.com/45HKXsE.gif"
    }
  }
  const positionClassName: string = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement: HTMLElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  // todo: show fighter info (image, name, health, etc.)
  const fighterImage: HTMLElement  = createFighterImage(fighter);
  const fighterName: HTMLElement = createElement('h2' as unknown as IElementInfo);
  const fighterHealth: HTMLElement = createElement('p' as unknown as IElementInfo);
  const fighterAttack: HTMLElement = createElement('p' as unknown as IElementInfo);
  const fighterDefense: HTMLElement = createElement('p' as unknown as IElementInfo);

  fighterName.append(fighter.name);
  fighterHealth.append(`Health: ${fighter.health}`);
  fighterAttack.append(`Attack: ${fighter.attack}`);
  fighterDefense.append(`Defense: ${fighter.defense}`);
  fighterElement.append(fighterName, fighterImage, fighterHealth, fighterAttack, fighterDefense);

  return fighterElement;
}

export function createFighterImage(fighter: IFighter): HTMLElement {
  const { source, name } = fighter;
  const attributes: IAttributes = {
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement: HTMLElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes
  });

  return imgElement;
}
