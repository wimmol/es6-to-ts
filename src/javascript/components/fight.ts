import { controls } from '../../constants/controls';
import { IFighter, IConditions } from "../helpers/tsTypes";


export async function fight(firstFighter: IFighter, secondFighter: IFighter): Promise<IFighter> {
  return new Promise((resolve) => {
    // resolve the promise with the winner when fight is over

    // константы
    const critTimer = 10000; // 10 секунд (10000 миллисекунд для функции setTimeout)
    const critFactor = 2; // множитель крита

    // подключаем eventListeners для прослушки событий нажатия на клавишу клавиатуры и ее отпускание
    document.addEventListener("keydown", logKeyDown);
    document.addEventListener("keyup", logKeyUp);

    // подключает dom елементы для работы с их стилями ( изменение  ширины healthBar)
    const leftFighterIndicator: HTMLElement = <HTMLElement>document.getElementById("left-fighter-indicator");
    const rightFighterIndicator: HTMLElement = <HTMLElement>document.getElementById("right-fighter-indicator");

    // создаем Set для хранения зажатых в данный момент кнопок
    let pressedKeys: Set<string> = new Set();

    // работа с eventListeners
    function logKeyDown(key: KeyboardEvent): void {
      if (key.repeat){
        return;
      }
      pressedKeys.add(key.code);
      hit(key);
      healthBarView();
      endFight();
    }
    function logKeyUp(key: KeyboardEvent): void {
      pressedKeys.delete(key.code);

      switch (key.code)   {
        case controls.PlayerOneBlock:
          conditions.isPlayerOneInBlock = false;
          break;
        case controls.PlayerTwoBlock:
          conditions.isPlayerTwoInBlock = false;
          break;
      }
    }

    // проверяем кд крита и нажаты ли все нужные кнопки  для него
    function isCritReady(pos: 'left' | 'right'): boolean{
      let keys: [string, string, string];
      let isReady: boolean;

      if (pos === 'left'){
        keys = controls.PlayerOneCriticalHitCombination;
        isReady = conditions.isPlayerOneCritReady;
      }
      else {
        keys = controls.PlayerTwoCriticalHitCombination;
        isReady = conditions.isPlayerTwoCritReady;
      }
      if (pressedKeys.has(keys[0]) &&
          pressedKeys.has(keys[1]) &&
          pressedKeys.has(keys[2]) &&
          isReady) {
        return true;
      }
      else {
        return false;
      }
    }

    // наносим крит и отправляем его в перезарядку
    function crit(attacker: IFighter, pos: 'left' | 'right'): void{
      if (pos === 'left') {
        conditions.playerTwoHealth -= attacker.attack * critFactor;
        conditions.isPlayerOneCritReady = false;
        setTimeout(() => { conditions.isPlayerOneCritReady = true }, critTimer);
      }
      else if (pos === 'right') {
        conditions.playerOneHealth -= attacker.attack * critFactor;
        conditions.isPlayerTwoCritReady = false;
        setTimeout(() => { conditions.isPlayerTwoCritReady = true }, critTimer);
      }
    }

    // обьект с переменными, которые хранят состаяния каждого из бойцов
    // по хорошему, нужно было дополнить обьекты бойцов, но понял это слишком поздно
    let conditions: IConditions= {
      isPlayerOneInBlock: false,
      isPlayerTwoInBlock: false,
      playerOneHealth: firstFighter.health,
      playerTwoHealth: secondFighter.health,
      playerOnePerHealth: 100,
      playerTwoPerHealth: 100,
      isPlayerOneCritReady: true,
      isPlayerTwoCritReady: true
    }

    // работа с healthBar  (пересчитываем его ширину)
    function healthBarView(): void {
      conditions.playerOnePerHealth = conditions.playerOneHealth  * 100 / firstFighter.health;
      conditions.playerTwoPerHealth = conditions.playerTwoHealth * 100 / secondFighter.health;
      if (conditions.playerOnePerHealth < 0) conditions.playerOnePerHealth = 0;
      if (conditions.playerTwoPerHealth < 0) conditions.playerTwoPerHealth = 0;
      leftFighterIndicator.style.width = `${conditions.playerOnePerHealth }%`;
      rightFighterIndicator.style.width = `${conditions.playerTwoPerHealth}%`;
    }

    // проверка конца боя
    function endFight(): void {
      if (conditions.playerOneHealth <= 0) {
        document.removeEventListener("keydown", logKeyDown);
        document.removeEventListener("keyup", logKeyUp);
        resolve(secondFighter);
      }

      if (conditions.playerTwoHealth <= 0){
        document.removeEventListener("keydown", logKeyDown);
        document.removeEventListener("keyup", logKeyUp);
        resolve(firstFighter);
      }
    }

    // обработка любого удара, если он вообще был
    function hit(key: KeyboardEvent) {
      if (isCritReady('left')) {
        crit(firstFighter, 'left');
      }
      if (isCritReady('right')) {
        crit(secondFighter, 'right');
      }
      switch (key.code) {
        case controls.PlayerOneAttack:
          if (conditions.isPlayerOneInBlock) break;

          if (!conditions.isPlayerTwoInBlock) {
            conditions.playerTwoHealth -= getDamage(firstFighter, secondFighter);
          }
          break;
        case controls.PlayerTwoAttack:
          if (conditions.isPlayerTwoInBlock) break;

          if (!conditions.isPlayerOneInBlock) {
            conditions.playerOneHealth -= getDamage(secondFighter, firstFighter);
          }
          break;
        case controls.PlayerOneBlock:
          conditions.isPlayerOneInBlock = true;
          break;
        case controls.PlayerTwoBlock:
          conditions.isPlayerTwoInBlock = true;
          break;
      }
    }
  });
}

export function getDamage(attacker: IFighter, defender: IFighter): number {
  // return damage
  const damage: number = getHitPower(attacker) - getBlockPower(defender);
  return damage > 0 ? damage : 0;
}

export function getHitPower(fighter: IFighter): number {
  // return hit power
  const criticalHitChance = Math.random() + 1;
  return fighter.attack * criticalHitChance;
}

export function getBlockPower(fighter: IFighter): number {
  // return block power
  const dodgeChance = Math.random() + 1;
  return fighter.defense * dodgeChance;
}
