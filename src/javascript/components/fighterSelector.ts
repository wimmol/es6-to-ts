import { createElement } from '../helpers/domHelper';
import { renderArena } from './arena';
// @ts-ignore
import versusImg from '../../../resources/versus.png';
import { createFighterPreview } from './fighterPreview';
import { fighterService } from "../services/fightersService";
import {IFighter, selector, twoFighters} from "../helpers/tsTypes";

export function createFightersSelector(): selector {
  let selectedFighters: twoFighters | [] = [];

  return async (event: Event, fighterId: string) => {
    const fighter: IFighter = await getFighterInfo(fighterId);
    const [playerOne, playerTwo] = selectedFighters;
    const firstFighter: IFighter = playerOne ?? fighter;
    const secondFighter: IFighter = <IFighter>(Boolean(playerOne) ? playerTwo ?? fighter : playerTwo);
    selectedFighters = [firstFighter, secondFighter];

    renderSelectedFighters(selectedFighters);
  };
}

const fighterDetailsMap = new Map();

export async function getFighterInfo(fighterId:string): Promise<IFighter> {
  // get fighter info from fighterDetailsMap or from service and write it to fighterDetailsMap
  const fighter: IFighter = fighterDetailsMap.get(fighterId);
  if (typeof fighter == "undefined") {
    await fighterService.getFighterDetails(fighterId)
        .then(fighter => { fighterDetailsMap.set(fighterId, fighter) });
  }
  return fighterDetailsMap.get(fighterId);

}

function renderSelectedFighters(selectedFighters: twoFighters): void {
  const fightersPreview: HTMLElement | null = document.querySelector('.preview-container___root');
  const [playerOne, playerTwo]: twoFighters = selectedFighters;
  const firstPreview: HTMLElement = createFighterPreview(playerOne, 'left');
  const secondPreview: HTMLElement = createFighterPreview(playerTwo, 'right');
  const versusBlock: HTMLElement = createVersusBlock(selectedFighters);
  if (fightersPreview !== null) {
    fightersPreview.innerHTML = '';
    fightersPreview.append(firstPreview, versusBlock, secondPreview);
  }
}

function createVersusBlock(selectedFighters: twoFighters) {
  const canStartFight: boolean = selectedFighters.filter(Boolean).length === 2;
  const onClick = () => startFight(selectedFighters);
  const container: HTMLElement = createElement({ tagName: 'div', className: 'preview-container___versus-block' });
  const image: HTMLElement = createElement({
    tagName: 'img',
    className: 'preview-container___versus-img',
    attributes: { src: versusImg },
  });
  const disabledBtn: string = canStartFight ? '' : 'disabled';
  const fightBtn: HTMLElement = createElement({
    tagName: 'button',
    className: `preview-container___fight-btn ${disabledBtn}`,
  });

  fightBtn.addEventListener('click', onClick, false);
  fightBtn.innerText = 'Fight';
  container.append(image, fightBtn);

  return container;
}

function startFight(selectedFighters: twoFighters) {
  renderArena(selectedFighters);
}
